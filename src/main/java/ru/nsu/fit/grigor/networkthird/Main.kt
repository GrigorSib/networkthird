package ru.nsu.fit.grigor.networkthird

import java.io.IOException
import java.net.SocketException
import java.net.UnknownHostException
import kotlin.system.exitProcess

fun main(args: Array<String>) { // name; port; lossPercent; optional: ip ; port;
    val node: TreeNode
    when (args.size) {
        5 -> {
            try {
                node = TreeNode(args[0], args[1].toInt(), args[2].toInt(), args[3], args[4].toInt())
            } catch (e: Exception) {
                e.printStackTrace()
                exitProcess(0)
            }
        }
        3 -> {
            try {
                node = TreeNode(args[0], args[1].toInt(), args[2].toInt())
            } catch (e: Exception) {
                e.printStackTrace()
                exitProcess(0)
            }
        }
        else -> {
            println("3 or 5 args!")
            exitProcess(0)
        }
    }
    try {
        node.connect()
        node.start()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}
