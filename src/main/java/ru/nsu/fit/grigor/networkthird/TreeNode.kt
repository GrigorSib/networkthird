package ru.nsu.fit.grigor.networkthird

import ru.nsu.fit.grigor.networkthird.messages.Message
import ru.nsu.fit.grigor.networkthird.messages.MessageNetworkReader
import ru.nsu.fit.grigor.networkthird.messages.MessageNetworkSender
import ru.nsu.fit.grigor.networkthird.messages.MessageType
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.*
import java.util.*
import kotlin.collections.HashMap

class TreeNode(val name: String,
               private val port: Int,
               private val lossPercent: Int,
               ipAddr: String? = null,
               mPort: Int? = null) {

    companion object {
        private const val MESSAGE_TIMEOUT = 5
        private const val MESSAGE_MSECOND_GAP = 2000
        private const val CONNECT_TIMEOUT = 1000
        private const val READ_MESSAGE_TIMEOUT = 100
    }

    private var sendSocket: MessageNetworkSender = MessageNetworkSender(DatagramSocket(port + 1))//todo:why

    private var readSocket: MessageNetworkReader = MessageNetworkReader(DatagramSocket(port))

    private val inputReader: BufferedReader = BufferedReader(InputStreamReader(System.`in`))

    private var messagesToSend = HashMap<Message, SendInfo>()

    private var neighbours = HashMap<InetSocketAddress, InetSocketAddress?>()

    private var reconnectNode: InetSocketAddress? = null

    private val isMessageLost: Boolean
        get() = Random().nextDouble() * 100 < lossPercent

    init {
        mPort?.let {
            neighbours[InetSocketAddress(InetAddress.getByName(ipAddr), mPort)] = null
            reconnectNode = InetSocketAddress(InetAddress.getByName(ipAddr), mPort)
        }
    }

    private fun findMessageByUUID(uuid: UUID): Message? {
        for ((key) in messagesToSend) {
            if (key.uuid == uuid) return key
        }
        return null
    }

    /**
     * Join reconnectNode and add it to neighbours
     * @throws IOException
     */
    @Throws(IOException::class)
    fun connect() {
        if (reconnectNode == null) return

        readSocket.enableTimeout(CONNECT_TIMEOUT)

        val registration = Message(port.toString(),
                MessageType.NewConnection,
                reconnectNode!!.address,
                reconnectNode!!.port)
        for (i in 0 until MESSAGE_TIMEOUT) {
            println("Trying to connect to first host...")
            sendSocket.sendMessage(registration)
            val delivered: Message? = readSocket.readMessage()
            if (delivered != null && delivered.messageType == MessageType.Delivered) {
                if (delivered.message.split(" ").size == 2) {
                    val nodeInfo = delivered.message.split(" ")
                    if (nodeInfo.size > 1) {
                        neighbours[reconnectNode!!] = InetSocketAddress(InetAddress.getByName(nodeInfo[0]), nodeInfo[1].toInt())
                    } else {
                        neighbours[reconnectNode!!] = null
                    }
                } else {
                    neighbours[reconnectNode!!] = null
                }
                println("Connection successful!")
                return
            }
        }
        println("Host timeout!")
        neighbours.remove(reconnectNode)
        reconnectNode = null
    }

    @Throws(IOException::class)
    private fun sendToNeighbours(_message: String, messageType: MessageType, notSendTo: InetSocketAddress? = null) {
        val message = StringBuilder().append(port).also {
            if (_message.isNotEmpty()) {
                it.append(" ").append(_message)
            }
        }.toString()
        for (neighb in neighbours.keys) {
            if (neighb != notSendTo) {
                val toSend = Message(message, messageType, neighb.address, neighb.port)
                sendSocket.sendMessage(toSend)
                messagesToSend[toSend] = SendInfo()
            }
        }
        println("Message sent!")
    }

    @Throws(IOException::class)
    private fun readMessage(msg: Message) {
        when (msg.messageType) {
            MessageType.Delivered -> {
                messagesToSend.remove(findMessageByUUID(UUID.fromString(msg.message)))
            }
            MessageType.Message -> {
                val messageContentArray = msg.message.split(" ", limit = 2)

                val portFrom = messageContentArray[0].toInt()
                val message = if (messageContentArray.size > 1) messageContentArray[1] else ""

                println("${msg.ipFrom}: $message")

                sendToNeighbours(message, MessageType.Message, InetSocketAddress(msg.ipFrom, portFrom))

                val deliveryConfirmation = Message(msg.uuid.toString(),
                        MessageType.Delivered,
                        msg.ipFrom,
                        portFrom)
                sendSocket.sendMessage(deliveryConfirmation)
            }
            MessageType.NodeUpdate -> {
                val updateInfo = msg.message.split(" ")
                if (updateInfo.size == 1) {
                    neighbours[InetSocketAddress(msg.ipFrom, updateInfo[0].toInt())] = null
                } else {
                    neighbours[InetSocketAddress(msg.ipFrom, updateInfo[0].toInt())] =
                            InetSocketAddress(InetAddress.getByName(updateInfo[1]), updateInfo[2].toInt())
                }
                val deliveryConfirm = Message(msg.uuid.toString(),
                        MessageType.Delivered,
                        msg.ipFrom,
                        updateInfo[0].toInt())
                sendSocket.sendMessage(deliveryConfirm)
                println("Node updated his reconnect info!")
            }
            MessageType.NewConnection -> {
                println("New neighbour connected!")
                val registerInfo = msg.message.trim()
                val newNeighbour = InetSocketAddress(msg.ipFrom, registerInfo.toInt())
                neighbours[newNeighbour] = null
                println("Sending confirmation to $newNeighbour")

                val reconnectInfo = StringBuilder()
                if (reconnectNode != null) {
                    reconnectInfo
                            .append(reconnectNode!!.address.toString().replaceFirst("/", ""))
                            .append(" ")
                            .append(reconnectNode!!.port)
                }
                sendSocket.sendMessage(
                        Message(reconnectInfo.toString(),
                                MessageType.Delivered,
                                msg.ipFrom,
                                msg.message.toInt()
                        )
                )

                if (reconnectNode == null && neighbours.size > 1) {
                    reconnectNode = neighbours.keys.first()
                    reconnectInfo.clear()
                            .append(reconnectNode!!.address.toString().replaceFirst("/", ""))
                            .append(" ")
                            .append(reconnectNode!!.port)

                    sendToNeighbours(reconnectInfo.toString(), MessageType.NodeUpdate, reconnectNode)
                }
            }
        }
    }

    private fun checkTimeout() {
        val messagesToRemove = ArrayList<Message>()
        val nodesToRemove = mutableSetOf<InetSocketAddress>()
        for ((key, time) in messagesToSend) {
            val span = System.currentTimeMillis() - time.startTimeMls
            if (span > MESSAGE_MSECOND_GAP * MESSAGE_TIMEOUT && time.tries == 5) {
                nodesToRemove.add(InetSocketAddress(key.ipFrom, key.portFrom))
                val some = messagesToSend.filterKeys { key1 ->
                    key.ipFrom == key1.ipFrom && key.portFrom == key1.portFrom
                }.keys
                messagesToRemove.addAll(
                        some
                )
                println("Node timed out!")
            } else if ((time.tries + 1) * MESSAGE_MSECOND_GAP < span) {
                time.tries++
                sendSocket.sendMessage(key)
                println("Trying to deliver message once again: try ${time.tries}")
            }
        }

        messagesToSend.keys.removeAll(messagesToRemove)
        if (nodesToRemove.isNotEmpty()) {
            println("nodes size ${nodesToRemove.size}")
            nodesToRemove.forEach {
                deleteNode(it)
            }
        }
    }

    private fun deleteNode(toDelete: InetSocketAddress) {
        val toConnect = neighbours.remove(toDelete)

        if (toDelete == reconnectNode) {
            var registerInfo = ""
            if (toConnect != null) {
                reconnectNode = toConnect
                connect()
            }

            if (neighbours.size > 1) {
                reconnectNode = neighbours.keys.first()
                registerInfo = reconnectNode!!.address.toString()
                        .replaceFirst("/", "") +
                        " " + reconnectNode!!.port
            } else {
                reconnectNode = null
            }

            sendToNeighbours(registerInfo, MessageType.NodeUpdate, reconnectNode)
        }
    }

    fun start() {
        println("Node is online")
        readSocket.enableTimeout(READ_MESSAGE_TIMEOUT)
        while (true) {
            try {
                readMessageAndNotify()
                readUserInput()
                checkTimeout()
            } catch (e: IOException) {
                e.printStackTrace()
                clearResources()
                break
            }
        }
    }

    private fun readUserInput() {
        if (inputReader.ready()) {
            val newMessage = inputReader.readLine()
            sendToNeighbours(newMessage, MessageType.Message)
        }
    }

    private fun readMessageAndNotify() {
        val msg: Message? = readSocket.readMessage()
        msg?.let {
            println("Got message from ${msg.ipFrom}  ${msg.portFrom-1}")
            if (!isMessageLost) {
                readMessage(msg)
            } else {
                println("Message's been \"lost\"")
            }
        }
    }


    private fun clearResources() {
        readSocket.closeSocket()
        sendSocket.closeSocket()
    }
}
