package ru.nsu.fit.grigor.networkthird.messages

import java.net.InetAddress
import java.util.*

class Message constructor(var message: String,
                          val messageType: MessageType,
                          var ipFrom: InetAddress,
                          var portFrom: Int,
                          _uuid: String? = null) {

    val uuid: UUID

    init {
        if (_uuid != null) {
            this.uuid = UUID.fromString(StringBuilder() // todo: wtf
                    .append(_uuid, 0, 8)
                    .append('-')
                    .append(_uuid, 8, 12)
                    .append('-')
                    .append(_uuid, 12, 16)
                    .append('-')
                    .append(_uuid, 16, 20)
                    .append('-')
                    .append(_uuid, 20, 32).toString())
        } else {
            this.uuid = UUID.randomUUID()
        }
    }
}