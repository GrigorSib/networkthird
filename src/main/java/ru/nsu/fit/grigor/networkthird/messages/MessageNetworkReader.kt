package ru.nsu.fit.grigor.networkthird.messages

import java.io.IOException
import java.math.BigInteger
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.*

class MessageNetworkReader(private val socket: DatagramSocket) {

    fun closeSocket() {
        socket.close()
    }

    @Throws(SocketException::class)
    fun enableTimeout(ms: Int) {
        socket.soTimeout = ms
    }

    @Throws(IOException::class)
    fun readMessage(): Message? {
        val msgPacket = DatagramPacket(ByteArray(65571), 65571)//todo: wtf?
        try {
            socket.receive(msgPacket)
        } catch (e: SocketTimeoutException) {
            return null
        }
        val uuid = Arrays.copyOfRange(msgPacket.data, 0, 32)
        val msgType = Arrays.copyOfRange(msgPacket.data, 32, 33)
        val msg = Arrays.copyOfRange(msgPacket.data, 33, msgPacket.data.size)
        return Message(
                String(msg).trim{
                    it == 0.toChar()
                },
                MessageType.valueOf(BigInteger(msgType).toInt()),
                msgPacket.address,
                msgPacket.port,
                String(uuid).trim()
        )
    }
}