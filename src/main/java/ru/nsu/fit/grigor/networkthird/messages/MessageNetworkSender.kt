package ru.nsu.fit.grigor.networkthird.messages

import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket

class MessageNetworkSender(private val datagramSocket: DatagramSocket) {
    fun closeSocket() {
        datagramSocket.close()
    }

    @Throws(IOException::class)
    fun sendMessage(message: Message) {
        val bUuid: ByteArray = message.uuid.toString().replace("-", "").toByteArray()
        val mt = message.messageType.type.toByte()
        val msg: ByteArray = message.message.toByteArray()
        val packet = DatagramPacket(bUuid.plus(mt).plus(msg),
                msg.size + bUuid.size + 1,
                message.ipFrom,
                message.portFrom)
        datagramSocket.send(packet)
    }
}