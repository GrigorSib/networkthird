package ru.nsu.fit.grigor.networkthird.messages

enum class MessageType(val type: Int) {
    Delivered(0),
    Message(1),
    NewConnection(2),
    NodeUpdate(3);

    companion object {
        fun valueOf(v: Int): MessageType {
            return when (v) {
                0 -> Delivered
                1 -> Message
                2 -> NewConnection
                3 -> NodeUpdate
                else -> throw IllegalArgumentException("No such MessageType with type=$v")
            }
        }
    }
}